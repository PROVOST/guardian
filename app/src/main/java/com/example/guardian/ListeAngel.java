package com.example.guardian;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;


public class ListeAngel extends AppCompatActivity {
    private ListView listAngel;
    private Button retourBtn;
    private ArrayList<String> angelList = new ArrayList<String>();
    private DBHelper db;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_angel);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, angelList);

        listAngel = (ListView) findViewById(R.id.ListView);
        listAngel.setAdapter(arrayAdapter);
        retourBtn = findViewById(R.id.retour_liste_angel);

        db = new DBHelper(this);
        Cursor res = db.getAngels();

        while (res.moveToNext()) {
            angelList.add(res.getString(1));
        }

        listAngel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                String nam = arrayAdapter.getItem(position).toString();
                Toast.makeText(ListeAngel.this, nam, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ListeAngel.this, Angel.class);
                intent.putExtra("name",nam);
                startActivity(intent);
            }
        });

        retourBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View view){
                Intent intent = new Intent(ListeAngel.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}